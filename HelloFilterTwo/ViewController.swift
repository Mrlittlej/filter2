//
//  ViewController.swift
//  HelloFilterTwo
//
//  Created by 123APP on 2016/5/8.
//  Copyright © 2016年 com.hsinten. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var PhotoFrame: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func PickFromGallery(sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .PhotoLibrary
        
        imagePicker.delegate = self
        
        self.presentViewController(imagePicker, animated: true, completion: nil)
        
    }
}

extension ViewController:UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        self.PhotoFrame.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismissViewControllerAnimated(false, completion: nil)
    }
}